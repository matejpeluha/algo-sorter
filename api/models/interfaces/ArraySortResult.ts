interface ArraySortResult{
    array: number[];
    time: number;
}

interface AllArraysResult{
    type: string;
    originalArray: number[];
    ts: ArraySortResult;
    cpp: ArraySortResult;

}

export {ArraySortResult, AllArraysResult};