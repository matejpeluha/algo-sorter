interface FactorialResult{
    executionTimeMs: number;
    result: number;
}

interface AllFactorialResults{
    nativeCppAddon: FactorialResult;
    typescript: FactorialResult;
}

export {FactorialResult, AllFactorialResults};