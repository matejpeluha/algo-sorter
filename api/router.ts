import {Router} from "express";
import {root} from "./routes/root";


const apiRouter: Router = Router();

apiRouter.use("/", root);

export {apiRouter};