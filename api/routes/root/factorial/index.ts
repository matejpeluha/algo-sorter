import {Router} from "express";
import {getParamRequest} from "./get.param.request";


const factorial: Router = Router();

factorial.get("/:number", getParamRequest);

export {factorial};