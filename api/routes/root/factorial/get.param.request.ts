import {Request, Response} from "express";
import {CalculatorService} from "../../../services/Calculator.service";

const getParamRequest = (req: Request, res: Response): void => {
    const calculator: CalculatorService =  new CalculatorService(req, res);
    calculator.getFactorialResult();
}

export {getParamRequest};