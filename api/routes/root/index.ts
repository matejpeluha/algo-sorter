import {Router} from "express";
import {getRequest} from "./get.request";
import {factorial} from "./factorial";
import {bubbleSort} from "./bubble-sort";

const root: Router = Router();

root.get("/", getRequest);

root.use("/factorial", factorial);
root.use("/bubble-sort", bubbleSort);

export {root};