import {Router} from "express";
import {getRequest} from "./get.request";


const bubbleSort: Router = Router();

bubbleSort.get("/:arraySize", getRequest);

export {bubbleSort};