import {Request, Response} from "express";
import {BubbleSorterService} from "../../../services/BubbleSorter.service";


const getRequest = (req: Request, res: Response): void => {
    const bubbleSorter: BubbleSorterService = new BubbleSorterService(req, res);
    bubbleSorter.sort();
}

export {getRequest};