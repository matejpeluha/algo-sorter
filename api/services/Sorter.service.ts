import {Request, Response} from "express";

abstract class SorterService{
    protected req: Request;
    protected res: Response;
    protected readonly arraySize: number;

    protected constructor(req: Request, res: Response) {
        this.req = req;
        this.res = res;
        this.arraySize = parseInt(req.params.arraySize, 10);
    }


    protected generateArray(): number[]{
        const array: number[] = [];

        for(let index: number = 0; index < this.arraySize; index++){
            const randomNumber = Math.floor(Math.random() * this.arraySize)
            array.push(randomNumber);
        }

        return array;
    }

}

export {SorterService};