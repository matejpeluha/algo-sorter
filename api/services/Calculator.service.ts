import {Request, Response} from "express";
import {speedMath} from "./add-ons/SpeedMath";
import {AllFactorialResults, FactorialResult} from "../models/interfaces/factorialResult";





class CalculatorService{
    private req: Request;
    private res: Response;

    constructor(req: Request, res: Response) {
        this.req = req;
        this.res = res;
    }

    public getFactorialResult(): void{
        const number: number = parseInt(this.req.params.number);
        const nativeResult: FactorialResult = this.getNativeFactorial(number);
        const tsResult: FactorialResult = this.getTypescriptFactorial(number);

        const factorialResult: AllFactorialResults = {
            nativeCppAddon: nativeResult,
            typescript: tsResult
        }

        this.res.json(factorialResult);
    }

    private getNativeFactorial(number: number): FactorialResult{
        const start: number = performance.now();

        const result: number = speedMath.getFactorial(number);

        const stop: number = performance.now();
        const executionTime = (stop - start) ;

        const native: FactorialResult = {
            executionTimeMs: executionTime,
            result: result
        }

        return native;
    }

    private getTypescriptFactorial(number: number): FactorialResult{
        const start: number = performance.now();

        const result: number = this.runTypescriptFactorial(number);

        const stop: number = performance.now();
        const executionTime = (stop - start) ;

        const ts: FactorialResult = {
            executionTimeMs: executionTime,
            result: result
        }

        return ts;
    }

    private runTypescriptFactorial(number: number): number{
        if(number <= 1){
            return 1;
        }

        return number * this.runTypescriptFactorial(number - 1);
    }

}

export {CalculatorService};