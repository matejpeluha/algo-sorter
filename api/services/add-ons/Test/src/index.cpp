#include "test.h"
#include "../node_modules/node-addon-api/napi.h"


Napi::String napiHelloUser(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	std::string name = (std::string)info[0].ToString();
	std::string result = helloUser(name);
	
	return Napi::String::New(env, result);
 }

Napi::Number napiAdd(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	int a = info[0].ToNumber();
	int b = info[1].ToNumber();
	int result = add(a, b);

	return Napi::Number::New(env, result);
}

Napi::Object Init(Napi::Env env, Napi::Object exports) {
	exports.Set("helloUser", Napi::Function::New(env, napiHelloUser));
	exports.Set("add", Napi::Function::New(env, napiAdd));

	return exports;
}


NODE_API_MODULE(test, Init)