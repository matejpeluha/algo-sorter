#include "SpeedMath.h"
#include "../../node_modules/node-addon-api/napi.h";

Napi::String napiGetFactorial(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	int number = info[0].ToNumber();
	std::string result = std::to_string(getFactorial(number));
	
	return Napi::String::New(env, result);
}


Napi::Object Init(Napi::Env env, Napi::Object exports) {

	exports.Set("getFactorial", Napi::Function::New(env, napiGetFactorial));

	return exports;
}


NODE_API_MODULE(speedmath, Init)