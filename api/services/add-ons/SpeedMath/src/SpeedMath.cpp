#include "SpeedMath.h"


unsigned long long getFactorial(int number) {
	if (number <= 1) {
		return 1;
	}

	return number * getFactorial(number - 1);
}