#include "Sorter.h";
#include "../../node_modules/node-addon-api/napi.h";



Napi::Array napiBubbleSort(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	const Napi::Array inputArray = info[0].As<Napi::Array>();
	const int arrayLength = inputArray.Length();

	std::vector<int>* array = new std::vector<int>();
	
	for (int i = 0; i < arrayLength; i++) {
		int number = inputArray[i].As<Napi::Number>().Uint32Value();
		array->push_back(number);
	}

	bubbleSort(array);

	Napi::Array outputArray = Napi::Array::New(env, arrayLength);
	for (int i = 0; i < arrayLength; i++) {
		outputArray[i] = Napi::Number::New(env, array->at(i));
	}


	return outputArray;
}


Napi::Object Init(Napi::Env env, Napi::Object exports) {
	exports.Set("bubbleSort", Napi::Function::New(env, napiBubbleSort));

	return exports;
}


NODE_API_MODULE(sorter, Init)