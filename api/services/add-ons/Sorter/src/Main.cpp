﻿// Sorter.cpp : Defines the entry point for the application.
//

#include "Sorter.h"
#include <iostream>;

using namespace std;

int main()
{
	std::vector<int> array = { -2, 45, 0, 11, -9 };

	bubbleSort(&array);
	
	for (int i = 0; i < array.size(); i++) {
		cout << "  " << array.at(i);
	}
	

	return 0;
}
