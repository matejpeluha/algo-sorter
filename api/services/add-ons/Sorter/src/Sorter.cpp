#include "Sorter.h"
#include <iostream>

void bubbleSort(std::vector<int>* array) {
    const int arraySize = array->size();
    for (int step = 0; step < (arraySize - 1); ++step) {

        // check if swapping occurs
        int swapped = 0;

        // loop to compare two elements
        for (int i = 0; i < (arraySize - step - 1); ++i) {

            // compare two array elements
            // change > to < to sort in descending order
            const int higherIndex = i + 1;
            if (array->at(i) > array->at(higherIndex)) {

                std::swap(array->at(i), array->at(higherIndex));

                swapped = 1;
            }
        }

        // no swapping means the array is already sorted
        // so no need of further comparison
        if (swapped == 0)
            break;
    }
}