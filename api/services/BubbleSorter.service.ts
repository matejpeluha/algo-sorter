import {Request, Response} from "express";
import {SorterService} from "./Sorter.service";
import {AllArraysResult, ArraySortResult} from "../models/interfaces/ArraySortResult";
import {sorter} from "./add-ons/Sorter";

class BubbleSorterService extends SorterService{

    constructor(req: Request, res: Response) {
        super(req, res);
    }

    public sort(){
        const originalArray: number[] = this.generateArray();
        const arrayForTs: number[] = [...originalArray];
        const arrayForCpp: number[] = [...originalArray];


        const tsResult: ArraySortResult = this.tsBubbleSortResult(arrayForTs);
        const cppResult: ArraySortResult = this.cppBubbleSortResult(arrayForCpp);

        const json: AllArraysResult = {
            type: "bubble-sort",
            originalArray: originalArray,
            ts: tsResult,
            cpp: cppResult
        }

        this.res.json(json);
    }

    private tsBubbleSortResult(array: number[]): ArraySortResult{
        const start: number = performance.now();

        const sortedArray: number[] = this.tsBubbleSortArray(array);

        const stop: number = performance.now();
        const time: number = stop - start;

        const result: ArraySortResult = {
            array: sortedArray,
            time: time
        }

        return result;
    }

    private tsBubbleSortArray(array: number[]): number[]{
        for (let step: number = 0; step < (array.length - 1); ++step) {

            // check if swapping occurs
            let swapped: number = 0;

            // loop to compare two elements
            for (let i: number = 0; i < (array.length - step - 1); ++i) {

                // compare two array elements
                // change > to < to sort in descending order
                if (array[i] > array[i + 1]) {

                    // swapping occurs if elements
                    // are not in intended order
                    const temp: number = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temp;

                    swapped = 1;
                }
            }

            // no swapping means the array is already sorted
            // so no need of further comparison
            if (swapped == 0)
                break;
        }

        return array;
    }


    private cppBubbleSortResult(array: number[]): ArraySortResult{
        const start: number = performance.now();

        const sortedArray: number[] = sorter.bubbleSort(array);

        const stop: number = performance.now();
        const time: number = stop - start;

        const result: ArraySortResult = {
            array: sortedArray,
            time: time
        }

        return result;
    }

}

export {BubbleSorterService};